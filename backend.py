from influxdb_client import InfluxDBClient, Point, WritePrecision
from datetime import datetime, timezone
import psutil
import time

# Vos informations d'authentification et de connexion à InfluxDB
token = "uzGXy7nSG-V9RuCfgDjvQMFpkts857mMh4NWQFX8DL-xhWE9UMXZjsUOFbjRQzQcpn1_Qds0K2E0ay-VfNXlaA=="
org = "School"
bucket = "Projet_DevSecOps"
url = "http://localhost:8086"

# Création du client InfluxDB
client = InfluxDBClient(url=url, token=token, org=org)
write_api = client.write_api()

try:
    while True:
        # Collecte des informations système avec psutil
        cpu_usage = psutil.cpu_percent()
        memory = psutil.virtual_memory()
        memory_usage = memory.percent  # Utilisation de la mémoire en pourcentage
        disk_usage = psutil.disk_usage('/').percent  # Utilisation du disque principal en pourcentage
        net_io = psutil.net_io_counters()
        bytes_sent = net_io.bytes_sent
        bytes_recv = net_io.bytes_recv

        # Création d'un point de données
        point = Point("system_metrics")\
            .tag("host", "local")\
            .field("cpu_usage", cpu_usage)\
            .field("memory_usage", memory_usage)\
            .field("disk_usage", disk_usage)\
            .field("bytes_sent", bytes_sent)\
            .field("bytes_recv", bytes_recv)\
            .time(datetime.now(timezone.utc), WritePrecision.NS)

        # Envoi du point à InfluxDB
        write_api.write(bucket=bucket, org=org, record=point)

        print("Données envoyées avec succès à InfluxDB.")

        # Attendre avant la prochaine collecte (ex : 5 secondes)
        time.sleep(5)

except KeyboardInterrupt:
    print("\nArrêt du script.")
finally:
    # Fermeture propre du client InfluxDB
    write_api.close()
    client.close()
