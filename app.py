import streamlit as st
from influxdb_client import InfluxDBClient
from datetime import datetime, timedelta
import pandas as pd
import plotly.express as px

# Paramètres de connexion à InfluxDB
token = "uzGXy7nSG-V9RuCfgDjvQMFpkts857mMh4NWQFX8DL-xhWE9UMXZjsUOFbjRQzQcpn1_Qds0K2E0ay-VfNXlaA=="
org = "School"
bucket = "Projet_DevSecOps"
url = "http://localhost:8086"

# Interface utilisateur pour les filtres de temps
time_options = {
    "15 dernières minutes": timedelta(minutes=15),
    "Dernière heure": timedelta(hours=1),
    "3 dernières heures": timedelta(hours=3),
    "6 dernières heures": timedelta(hours=6),
    "12 dernières heures": timedelta(hours=12),
    "24 dernières heures": timedelta(hours=24),
}
selected_time = st.sidebar.selectbox("Sélectionnez la plage de temps:", list(time_options.keys()))

# Filtre de métriques
metric_options = ['cpu_usage', 'memory_usage', 'disk_usage']
selected_metrics = st.sidebar.multiselect('Sélectionnez les métriques à afficher:', metric_options, default=metric_options)

# Calcul de la période de temps sélectionnée
start_time = datetime.utcnow() - time_options[selected_time]

# Initialisation du client InfluxDB et création de la requête Flux
client = InfluxDBClient(url=url, token=token, org=org)

# Génération dynamique de la condition de filtre en fonction des métriques sélectionnées
metrics_filter = " or ".join([f'r._field == "{metric}"' for metric in selected_metrics])
query = f'''
from(bucket: "{bucket}")
  |> range(start: {start_time.isoformat()}Z)
  |> filter(fn: (r) => r._measurement == "system_metrics" and ({metrics_filter}))
  |> pivot(rowKey:["_time"], columnKey: ["_field"], valueColumn: "_value")
'''

# Exécution de la requête
result = client.query_api().query_data_frame(query=query)
client.close()  # Fermeture de la connexion à la base de données

# Traitement et affichage des données
st.title('Visualisation des données système')

if not result.empty:
    result['_time'] = pd.to_datetime(result['_time'])  # Conversion du temps en datetime
    
    # Création et affichage des graphiques avec l'axe Y fixé à 0-100%
    for metric in selected_metrics:
        if metric in result.columns:
            fig = px.line(result, x='_time', y=metric, title=f'Utilisation de {metric} (%)',
                          labels={"_time": "Temps", metric: "Pourcentage"}, range_y=[0,100])
            fig.update_layout(yaxis_title="Pourcentage (%)", xaxis_title="Temps")
            fig.update_yaxes(tickvals=[0, 20, 40, 60, 80, 100])
            st.plotly_chart(fig)
else:
    st.write("Aucune donnée disponible pour la période sélectionnée.")
